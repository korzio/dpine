define([
	'services/index',
	'directives/index',
	'views/index'
], function(){
	angular.module('dpine', [
  		'ngRoute',
  		'dpine.services',
  		'dpine.directives',
  		'dpine.views'
	])
	.config(['$routeProvider', function($routeProvider) {
	  	$routeProvider
	  		.when('/description/', {
	  			templateUrl: 'scripts/views/main/description.html'
	  		})
	  		.when('/chart-:index/', {
	  			templateUrl: 'scripts/views/main/chart.html'
	  		})
  			.when('/', {
				templateUrl: 'scripts/views/main/index.html'
			})
			.otherwise({ redirectTo: '/' });
	}]);

    angular.bootstrap(document.body, ['dpine']);
});