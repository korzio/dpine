define(['app/services'], function(services){
	services.factory('data', ['$http', '$q', function($http, $q){
		var data;

		function load(){
			return $http.get('scripts/data/charts.json')
				.then(function(response){
					data = response.data;
					return data;	
				});
		}

		return {
			get: function(){
				return $q.when(data || load());
			}
		}
	}]);
})