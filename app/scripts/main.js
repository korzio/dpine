/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        highcharts: {
            deps: [
                'jquery'
            ]
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        'app/app': {
            deps: [
                'angular',
                'angular-route'
            ]
        },
        'angular-route': {
            deps: [
                'angular'
            ]
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        highcharts: '../bower_components/highcharts-release/highcharts.src',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        angular: '../bower_components/angular/angular',
        'angular-route': '../bower_components/angular-route/angular-route',
        app: './app',
        text: '../bower_components/requirejs-text/text'
    }
});

require([
    'app/app'
]);
