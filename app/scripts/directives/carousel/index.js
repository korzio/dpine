define([
	'underscore',
	'app/directives',
    'text!directives/carousel/index.html'
], function(_, directives, tpl){
	directives.directive('carousel', ['$compile', function($compile) {
	  	return {
	  		restrict: 'A',
	    	link: function(scope, element){
    			var children, widths, containerWidth;

            	element.addClass('carousel carousel-container');
                element.append($compile(tpl)(scope));
                var serviceElementhsLength = element.children().length;

                scope.currentIndex = 0;
                scope.length = 0;

                scope.next = function(){
            		var firstChild = children[0],
            			currentElementMargin = widths[++scope.currentIndex];

        			firstChild.style.marginLeft = -currentElementMargin + 'px';
                };

                scope.prev = function(){
					var firstChild = children[0],
            			currentElementMargin = widths[--scope.currentIndex];

        			firstChild.style.marginLeft = -currentElementMargin + 'px';
                };

                var removeWatch = scope.$watch(function(){
                	return element.children().length;
                }, reflow);

                window.onresize = reflow;

                function reflow(length){
                	length = element.children().length;
            		if(!length || length === serviceElementhsLength) {
            			return;
            		}

            		children = element.children();
            		
					var offsetWidth = 0;
					containerWidth = element[0].offsetWidth;
					widths = _.map(children, function(element){
						offsetWidth += element.offsetWidth;						
						return offsetWidth;
					});
					widths.unshift(0);

					var maxMargin = widths[widths.length - 1] - containerWidth;
					for(var i = widths.length - 1; i >= 0; i--) {
						if(widths[i] < maxMargin) {
							break;
						}

						widths[i] = maxMargin;						
					}
					scope.length = i + 1;

            		removeWatch && removeWatch();
                }
	    	}
	  	};
	}]);
})