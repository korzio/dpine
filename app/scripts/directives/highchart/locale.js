define({
	"menu": {
		"edit": {
			"text": "Edit",
			"title": "Click to edit",
			"className": "typcn typcn-edit"
		},
		"link": {
			"text": "Open",
			"title": "Open in a new document",
			"className": "typcn typcn-arrow-forward-outline"
		}
	}
});