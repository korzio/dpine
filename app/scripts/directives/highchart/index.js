define([
	'app/directives',
	'highcharts',
	'./locale'
], function(directives, highcharts, locale){

	var config = {
		actionEdit: '.highchart-action-edit',
		title: 'highcharts-title',
		titleHover: 'highchart-title-hover',
		renderTo: '.highchart-wrapper'
	};

	directives.directive('highchart', ['$timeout', '$routeParams', function($timeout, $routeParams) {
	  	return {
	    	templateUrl: 'scripts/directives/highchart/index.html',
	    	link: function(scope, element){
    			var removeWatch = scope.$watch('chartConfig', init);

	    		function init(){
    				if(!scope.chartConfig){
    					return;
    				}
    				removeWatch();

	    			scope.menu = _.clone(locale.menu);
	    			if($routeParams.index) {
	    				delete scope.menu.link;
	    			}

	    			scope.chart = new Highcharts.Chart(_.extend(scope.chartConfig, {
						chart: {
							renderTo: element[0].querySelector(config.renderTo)
						}
	    			}));

	    			scope.action = {
	    				on: function(action){
	    					this.type = action;
	    				},
	    				off: function(){
	    					delete this.type;

	    					// custom off
	    					element.removeClass(config.titleHover);
	    				},
	    				model: scope.chart.userOptions.title.text
	    			};

	    			scope.$watch('action.model', function(value){
						scope.chart.setTitle({ text: value.slice(0, 20) });
	    			});

	    			scope.eventEdit = function($event){
	    				if($event.keyCode === 13) {
	    					scope.chart.setTitle({ text: scope.action.model.slice(0, 20) });

	    					var elToFocus = element[0].querySelector(config.actionEdit);
							elToFocus && elToFocus.blur();
	    				}
	    			};

	    			scope.focusEditInput = function(){
						$timeout(function(){
							var elToFocus = element[0].querySelector(config.actionEdit);
							elToFocus && elToFocus.focus();
						})
	    			};

					// listeners
	    			scope.chart.container.addEventListener('click', function(event) {
						if(isTitleClicked(event)){
							scope.action.on('edit');
							event.stopPropagation();
							scope.focusEditInput();
						} else {
							scope.action.off();
							scope.$digest();
						}
	    			});

	    			scope.chart.container.addEventListener('mouseover', function(event) {
						if(isTitleClicked(event)){
							element.addClass(config.titleHover);
						}
					});

					scope.chart.container.addEventListener('mouseout', function(event) {
						if(isTitleClicked(event)){
							element.removeClass(config.titleHover);
						}
					});

					function isTitleClicked(event){
						var target = event.target.tagName === 'tspan' ? event.target.parentNode : event.target;
						return target && target.className.baseVal.indexOf(config.title) !== -1;
					}
	    		}
	    	}
	  	};
	}])
})