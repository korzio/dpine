define(['app/views'], function(controllers){
	controllers.controller('viewsMain', ['data', '$scope', function(dataService, $scope){
		dataService.get().then(function(charts){
			$scope.items = charts;
		});
	}]);
})