define(['app/views'], function(controllers){
	controllers.controller('viewsMainChart', ['data', '$scope', '$routeParams', function(dataService, $scope, $routeParams){
		dataService.get().then(function(charts){
			$scope.chartConfig = charts[$routeParams.index] || charts[0];
		});
	}]);
})